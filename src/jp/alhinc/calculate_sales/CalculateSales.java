package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";

	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";

	private static final String SERIAL_NUMBER = "売り上げファイルが連番になっていません";
	private static final String NUMBER_LESS = "合計金額が10桁を超えました";

	private static final String CODE_INJUSTICE = "の支店コードが不正です";
	private static final String COMMODITY_INJUSTICE = "の商品コードが不正です";
	private static final String FORMAT_INJUSTICE = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "^[0-9]{3}+$", FILE_INVALID_FORMAT, FILE_NOT_EXIST)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_COMMODITY_LST, commodityNames, commoditySales, "商品", "^[a-zA-Z0-9]{8}+$", FILE_INVALID_FORMAT, FILE_NOT_EXIST)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//各ファイル読み込み
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			//売り上げファイルの確認
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//連番かどうか確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(SERIAL_NUMBER);
				return;
			}
		}

		//ファイル読み込み
		String saleLine;
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader salebr = null;
			try {
				FileReader saleFr = new FileReader(rcdFiles.get(i));
				salebr = new BufferedReader(saleFr);
				List<String> saleItems = new ArrayList<>();

				//一行ずつ読み込んでsaleItemsに入れる
				while((saleLine = salebr.readLine()) != null) {
					saleItems.add(saleLine);
				}
				//ファイル内行数の確認
				if(saleItems.size() != 3){
					System.out.println(rcdFiles.get(i).getName() + FORMAT_INJUSTICE);
					return;
				}
				//支店コードの確認
				if(!branchNames.containsKey(saleItems.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + CODE_INJUSTICE);
					return;
				}
				//商品コードの確認
				if(!commodityNames.containsKey(saleItems.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_INJUSTICE);
					return;
				}
				//金額が数字だけかどうか
				if(!saleItems.get(2).matches("^[0-9]+$")){
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//saleItemsに入っている金額をlong型に変更しMapにいれる
				long fileSale = Long.parseLong(saleItems.get(2));
				Long saleAmount = branchSales.get(saleItems.get(0)) + fileSale;
				Long saleComAmount = commoditySales.get(saleItems.get(1)) + fileSale;

				//10桁以上になっていないか確認
				if(saleAmount >= 1000000000L) {
					System.out.println(NUMBER_LESS);
					return;
				}
				if(saleComAmount >= 1000000000L) {
					System.out.println(NUMBER_LESS);
					return;
				}
				branchSales.put(saleItems.get(0), saleAmount);
				commoditySales.put(saleItems.get(1), saleComAmount);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(salebr != null) {
					try {
						// ファイルを閉じる
						salebr.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}


		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		//商品別集計ファイルの書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}


	}


	/**
	 * 定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名、商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額、商品コードと金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales,String each, String regular, String invalit, String fileNot) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if(!file.exists()) {
				System.out.println(each + fileNot);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if((items.length != 2) || (!items[0].matches(regular))) {
					System.out.println(each + invalit);
					return false;
				}
				//Map格納
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名,商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額、商品コードと金額を保持するMap
	 * @return 書き込み可否
	 * @throws IOException
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) throws IOException {
		BufferedWriter bw = null;
		try {
			//ファイル書き込み
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//支店コード、支店名、金額で書きこむ
			for(String key:names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
